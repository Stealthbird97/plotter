import glob
import itertools
from natsort import natsorted
fluxcon = 551
def ccd2photon(x):
    return x*fluxcon/(1E4)
def photon2ccd(x):
    return x/fluxcon*(1E4)
def ccd2photon2(x):
    return x*fluxcon
def photon2ccd2(x):
    return x/fluxcon

def filebrowser(dir=".",ext=".asc"): #get list of ascii files
    "Returns files with an extension"
    return [f for f in glob.glob(f"{dir}\*{ext}", recursive=True)]

def groupfiles(filelist,grouplen): #Sorts file list into group of files of the same device
    groups = [list(g) for _, g in itertools.groupby(natsorted(filelist, reverse=True), lambda  x: x[0:grouplen])]
    return groups