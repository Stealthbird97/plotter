import errno
import os
import sys
from datetime import datetime
import numpy as np
import pandas as pd
import xarray as xr
import re
from scipy.signal import find_peaks
import matplotlib as matplotlib
import matplotlib.pyplot as plt
import itertools
from fuzzywuzzy import process
plt.rcParams.update({
    "text.usetex": True,
    "font.family": "sans-serif",
    'text.latex.preamble' : r'\usepackage{amsmath}'})
import scipy.signal as sig
import scipy.ndimage.filters as filt

from numba import jit
from scipy.optimize import curve_fit, minimize
from scipy.interpolate import interp1d
from auxfunctions import *

def _1Lorentzian(x, amp1, cen1, wid1):
    return (amp1*wid1**2/((x-cen1)**2+wid1**2))

#spectrafiles = ["ASCII/SS_R2C7_1s_1acq_53p0uW_780nm_laser_800FEL_4K.asc","ASCII/SS_R2C7_1s_1acq_62p0uW_780nm_laser_800FEL_4K.asc"]
indexes = "R2C7"
powers = [53,62]

def preparefiles(grouplength):
    files = filebrowser("ASCII", ".asc")
    files = natsorted(files, reverse=True)
    groups = groupfiles(files, grouplength)
    return files, groups

def parse_filenames(filename):
    filename = filename[6:] # remove folder name. Currently 'ASCII/' Should detect this automatically at some point
    #p = re.compile("(?P<name>\w*)_(?P<device>[BG]*\d*.*\d*)_(?P<int>\d*)s_(?P<acqs>\d*)acq_(?P<power>\d*p*\d*)uW_(?P<excite>\d*)nm.*_(?P<temp>\d*p*\d*)K(?:_(?P<BS>BSout))?(?:_(?P<pol>\d*)P)?")
    p = re.compile(
        "(?P<name>\w*)_(?P<device>[BG]*\d*.*\d*)_(?P<series>I+)_(?P<int>\d*)s_(?P<power>\d*p*\d*)uW_(?P<excite>\d*)nm.*_(?P<temp>\d*p*\d*)K(?:_(?P<BS>BSout))?(?:_(?P<pol>\d*)P)?")
    convertpower = re.compile('p')
    parsed = p.match(filename)
    samplename = parsed.group('name')
    devicename = parsed.group('device')
    int = parsed.group('int')
    int = float(convertpower.sub(".", int))
    acqs = 1 #float(parsed.group('acqs'))
    power = parsed.group('power')
    temp = parsed.group('temp')
    power = float(convertpower.sub(".", power))
    temp = float(convertpower.sub(".", temp))
    excite = float(parsed.group('excite'))
    filters = None
    BS = False
    try:
        if parsed.group('BS') == "BSout":
            BS = True
        else:
            BS = False
    except:
        BS = False
    try:
        if parsed.group('pol') != None:
            polarisation = str(parsed.group(9))
        else:
            polarisation = None
    except:
        polarisation = None

    parseddict = {
        "samplename": samplename,
        "devicename": devicename,
        "integration": int,
        "acquissions": acqs,
        "power": power,
        "temp": temp,
        "filter": filters,
        "excitation": excite,
        "BS": BS,
        "polarisation": polarisation
    }
    return parseddict

def importspectra(spectrafile):
    df = pd.DataFrame(columns=['Device', 'Power', 'Spectra'])
    for n in range(0, len(spectrafile)):
        parsed = parse_filenames(spectrafile[n])
        data = pd.read_csv(spectrafile[n], names=["wavelength", "counts"], skiprows=37)
        dataframe = pd.DataFrame([[parsed['devicename'], parsed['power'], parsed['temp'], parsed['excitation'],data]],columns=['Device', 'Power', 'Temp', 'Excitation', 'Spectra'])
        df = df.append(dataframe)
    #df = df.set_index(['Device', 'Power'])
    return df

def findpeaks(x):
    ofset=x.index[0]
    peaks, _ = find_peaks(x, prominence=0.2*np.max(x))
    plt.plot(peaks+ofset, x.iloc[peaks], "ob");
    plt.plot(x);
    plt.legend(['prominence'])
    plt.show()
    return peaks, _

def fitpeaks(spectra, peaks):
    ofset = 0
    j = 0
    fits = []
    cov = []
    perr = []
    for i in peaks:
        plt.plot(spectradata['wavelength'].iloc[i - 5:i + 5], spectradata['counts'].iloc[i - 5:i + 5])
        plt.title(str(j))
        plt.show()
        fitmax = np.max(spectra['counts'].iloc[i-5:i+5])
        popt, pcov = curve_fit(_1Lorentzian, spectra['wavelength'].iloc[i-5:i+5], spectra['counts'].iloc[i-5:i+5], p0=[0.5*fitmax, i, 0.001],bounds=(0,[10*fitmax,np.inf,10]), maxfev=1800)
        err = np.sqrt(np.diag(pcov))
        print(j, popt, pcov, err)
        fits.append(popt)
        cov.append(cov)
        perr.append(err)
        j+=1
    return fits, perr, cov

def generateplots(spectradata, name, power, excite):
    fig,ax1 = plt.subplots()
    ax1.set_title("Device {}, Power {}uW, Excite WL {}nm".format(name, power, excite))
    ax1.set_xlabel("Wavelength (nm)")
    ax1.set_ylabel(r'Counts at CCD (s$^{-1}$)')
    secax = ax1.secondary_yaxis('right', functions=(ccd2photon2, photon2ccd2))
    secax.set_ylabel(r'Photon Flux (s$^{-1}$)')
    ax1.plot(spectradata['wavelength'], spectradata['counts'], linewidth=0.5)
    plt.tight_layout()
    savename = "{}_{}_{}".format(name,excite,power)
    fig.savefig(savename + ".png", dpi=300)
    fig.savefig(savename + ".pdf", dpi=300)
    #fig.show()
    plt.close()
    return 1

def preselectpeaks(i=0):
    spectradata = data.iat[i, 2]
    peaks, peaksinfo = findpeaks(spectradata['counts'])
    peakswl = spectradata['wavelength'][peaks]
    return peaks, peaksinfo, peakswl

def getpeakheights(spectra, peaks):
    heights = []
    for i in peaks:
        heights.append(spectradata['counts'][i])
    heights = np.array(heights)
    return heights

files, groups = preparefiles(15)
data = importspectra(files)
autofindpeaks = True
fitpeaks = False
peaks = []
peaks, peaksinfo, peakswl = preselectpeaks(10)

peaksarray = np.empty([len(files), len(peaks)])
for i in range(0, len(files)):
    spectradata = data.iat[i,2]
    name = data.iat[i,0]
    power = data.iat[i,1]
    excite = data.iat[i,4]
    cropped = spectradata['counts']
    peaksarray[i]  = getpeakheights(spectradata, peaks)
    #print(peaksarray[i])
    #if fitpeaks == True:
    #   pars, errs, covs = fitpeaks(spectradata, peaks) # just the first peak
    #else:
    #    print('test')
    #j=0
    #plt.plot(spectradata['wavelength'], spectradata['counts'], linewidth=0.5)
    #for i in peaks:
    #    if errs[j][2]<10:
    #        plt.plot(spectradata['wavelength'], _1Lorentzian(spectradata['wavelength'], *pars[j]), linewidth=0.5, label=str(j))
    #    j+=1
    #plt.legend()
    #plt.show()
    #fitpeaks(spectradata, peaks2)
    #generateplots(spectradata, name, power, excite)
peaksarray = pd.DataFrame(peaksarray, data['Power'], peakswl)
markers = itertools.cycle(['o','s', 'p', '*', 'H', 'D', 'P', 'X', 'v', '^', '<', '>'])
for col in peaksarray.columns:
    plt.scatter(peaksarray.index, peaksarray.loc[:,col],label="{:.2f}nm".format(col),marker=next(markers))
plt.legend()
plt.show()